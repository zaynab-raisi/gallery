import Vue from 'vue';
import VueRouter from 'vue-router';
import { authGuard } from "./auth/authGuard";
import Home from './pages/Home';
import About from './pages/About';
import ArtistSingle from './pages/ArtistSingle';

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/about',
            component: About
        },
        {
            path: '/artist/:id',
            component: ArtistSingle,
            beforeEnter: authGuard
        }
    ]
})